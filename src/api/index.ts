import { Router } from "express";
import { CardRouter } from "../components/card/router";
import { UserRouter } from "../components/user/router";
import { AuthRouter } from "../components/auth/router";
const router: Router = Router();

router.use("/cards", CardRouter);
router.use("/users", UserRouter);
router.use("/authorization", AuthRouter);

export const api = router;
