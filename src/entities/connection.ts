import { createConnection, ConnectionOptions } from "typeorm";

const options: ConnectionOptions = {
  type: "mysql",
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  entities: [__dirname + "/*.js"],
  synchronize: Boolean(process.env.DB_SYNCHRONIZE),
  logging: Boolean(process.env.DB_LOGGING),
};

console.log('DB options', options);

export const connection = createConnection(options);
