import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";

enum Role {
  ADMIN = "admin",
  SUPERVISOR = "supervisor",
  BASIC = "basic",
}

@Entity({ name: "users" })
export class User {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ name: "email", unique: true, nullable: false })
  email: string;

  @Column({ name: "password", nullable: false, select: false })
  password: string;

  @Column({
    name: "role",
    type: "enum",
    enum: Role,
    default: Role.BASIC,
  })
  role: Role;

  @Column({ name: "firstName", nullable: true })
  firstName: string;

  @Column({ name: "lastName", nullable: true })
  lastName: string;

  @CreateDateColumn({ name: "createdAt" })
  createdAt: Date;

  @UpdateDateColumn({ name: "updatedAt" })
  updatedAt: Date;
}
