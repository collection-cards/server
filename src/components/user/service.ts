import { getRepository } from "typeorm";
import { User } from "../../entities/User";

export const service = {
  create: async (userData) => {
    const response = await getRepository(User).save(userData);
    return response;
  },

  getAll: async () => {
    const repo = getRepository(User).createQueryBuilder("users");
    const [list, totalCount] = await Promise.all([
      repo.getMany(),
      repo.getCount(),
    ]);
    return { list, totalCount };
  },

  getOneById: async (userId: string) => {
    const user = await getRepository(User)
      .createQueryBuilder("user")
      .where(`id=:id`, { id: userId })
      .getOne();
    return user;
  },

  deleteOneById: async (userId: string) => {
    const user = await getRepository(User)
      .createQueryBuilder("user")
      .where(`id=:id`, { id: userId })
      .getOne();
    return user;
  },
};
