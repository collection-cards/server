import { hashHelper } from "../../helpers/hash.helper";
import { jwtHelper } from "../../helpers/jwt.helper";
import { service as userService } from "./service";

export const controller = {
  create: async (req, res, next) => {
    try {
      const userData = {
        ...req.body,
        password: hashHelper.getPasswordHash(req.body.password),
      };
      const user = await userService.create(userData);
      const token = jwtHelper.generateToken(user);
      res.status(201).json({ user, token });
    } catch (error) {
      console.log(error);
    }
  },

  getAll: async (req, res, next) => {
    try {
      const list = await userService.getAll();
      res.status(200).json(list);
    } catch (error) {
      console.log(error);
    }
  },

  getOne: async (req, res, next) => {
    try {
      const user = await userService.getOneById(req.params.userId);
      res.status(200).json(user);
    } catch (error) {
      console.log(error);
    }
  },

  getProfileData: async (req, res, next) => {
    try {
      const user = await userService.getOneById(req.currentUser.id);
      res.status(200).json(user);
    } catch (error) {
      console.log(error);
    }
  },

  deleteOne: async (req, res, next) => {
    try {
      const response = await userService.deleteOneById(req.params.userId);
      res.status(200).json(response);
    } catch (error) {
      console.log(error);
    }
  },
};
