import { Router } from "express";
import { controller as userController } from "./controller";
import Joi from "express-joi-validation";
import { registration } from "./validator";
import { isAuthorized } from "../../middlewares/is-authorized";

const validator = Joi.createValidator({});

const router: Router = Router();

router.post("/", validator.body(registration), userController.create);
router.get("/", userController.getAll);
router.get("/profile", isAuthorized, userController.getProfileData);
// router.delete("/profile", isAuthorized, userController.delete);
router.get("/:userId", userController.getOne);
router.delete("/:userId", isAuthorized, userController.deleteOne);

export const UserRouter = router;
