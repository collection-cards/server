import Joi from "joi";

export const registration = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});
