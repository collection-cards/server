import { getRepository } from "typeorm";
import { Card } from "../../entities/Card";

export const service = {
  getAll: async (pagination, order, searchQuery) => {
    const repo = getRepository(Card).createQueryBuilder("cards");
    if (searchQuery) {
      repo.where(`cards.${searchQuery.searchby} like :search`, {
        search: `%${searchQuery.search}%`,
      });
      // .printSql();
    }
    const [list, totalCount] = await Promise.all([
      repo
        .orderBy(order.sortby, order.sort)
        .offset(pagination.offset)
        .limit(pagination.limit)
        .getMany(),
      repo.getCount(),
    ]);
    return { list, totalCount };
  },

  getOneById: async (cardId: string) => {
    const repo = await getRepository(Card)
      .createQueryBuilder("card")
      .where(`id=:id`, { id: cardId })
      .getOne();
    return repo;
  },
};
