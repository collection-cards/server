import { service as cardService } from "./service";
import { paginationHelper } from "../../helpers/pagination.helper";
import { sortHelper } from "../../helpers/sort.helper";
import { searchHelper } from "../../helpers/search.helper";

const sortWhiteList = ["name", "price", "id"];
const searchWhiteList = ["name", "status", "gender"];

export const controller = {
  getAll: async (req, res, next) => {
    try {
      const pagination = paginationHelper(req.query);
      const sort = sortHelper(req.query, sortWhiteList);
      const search = searchHelper(req.query, searchWhiteList);
      const result = await cardService.getAll(pagination, sort, search);
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
    }
  },

  getOne: async (req, res, next) => {
    try {
      const card = await cardService.getOneById(req.params.cardId);
      res.status(200).json(card);
    } catch (error) {
      console.log(error);
    }
  }
};
