import { Router } from "express";
import { controller as cardController } from "./controller";
import Joi from "express-joi-validation";
import { queryParams } from "./validator";

const validator = Joi.createValidator({});

const router: Router = Router();

router.get("/", validator.query(queryParams), cardController.getAll);
router.get("/:cardId", cardController.getOne);

export const CardRouter = router;
