import Joi from "joi";

export const queryParams = Joi.object({
  limit: Joi.number().positive(),
  page: Joi.number().positive(),
});
