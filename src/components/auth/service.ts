import { getRepository } from "typeorm";
import { User } from "../../entities/User";

export const service = {
  getOne: async (credentials) => {
    const response = await getRepository(User).findOne(credentials);
    return response;
  },
};
