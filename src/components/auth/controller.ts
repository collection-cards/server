import { hashHelper } from "../../helpers/hash.helper";
import { jwtHelper } from "../../helpers/jwt.helper";
import { service as authService } from "./service";

export const controller = {
  create: async (req, res, next) => {
    try {
      const credentials = {
        ...req.body,
        password: hashHelper.getPasswordHash(req.body.password),
      };
      const user = await authService.getOne(credentials);
      if (!user) {
        return res.status(400).send();
      }
      const token = jwtHelper.generateToken(user);
      res.status(201).json({ user, token });
    } catch (error) {
      console.log(error);
    }
  },

  check: async (req, res, next) => {
    try {
      const token = jwtHelper.generateToken(req.currentUser);
      res.status(200).json({ user: req.currentUser, token });
    } catch (error) {
      console.log(error);
    }
  },
};
