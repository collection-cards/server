import { Router } from "express";
import { controller as authController } from "./controller";
import Joi from "express-joi-validation";
import { authorization } from "./validator";
import { isAuthorized } from "../../middlewares/is-authorized";

const validator = Joi.createValidator({});

const router: Router = Router();

router.post("/", validator.body(authorization), authController.create);
router.get("/", isAuthorized, authController.check);

export const AuthRouter = router;
