import Joi from "joi";

export const authorization = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});
