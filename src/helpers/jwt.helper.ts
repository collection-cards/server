import jwt from "jsonwebtoken";
const signature = "MySuP3R_z3kr3t";

export const jwtHelper = {
  generateToken(user) {
    const expiration = "7d";
    return jwt.sign({ ...user }, signature, { expiresIn: expiration });
  },

  verifyToken(token) {
    return jwt.verify(token, signature);
  },
};
