const maxLimit = 45;
const minLimit = 9;

export const paginationHelper = ({ page, limit = minLimit }) => {
  limit = limit > maxLimit ? maxLimit : limit;
  const offset = page && page > 0 ? (page - 1) * limit : 0;

  return { limit, offset };
};
