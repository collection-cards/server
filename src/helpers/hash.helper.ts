import crypto from "crypto";

const tokenKey = "1a2b-3c4d-5e6f-7g8h";

export const hashHelper = {
  getPasswordHash(password) {
    return crypto
      .createHmac("sha256", tokenKey)
      .update(password + tokenKey)
      .digest("hex");
  },
};
