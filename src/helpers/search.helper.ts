export const searchHelper = (params, searchWhiteList: Array<string>) => {
  if (!params.search) {
    return null;
  }
  const search = params.search;
  const searchby = searchWhiteList.includes(params.searchby)
    ? params.searchby
    : "name";

  return { search, searchby };
};
