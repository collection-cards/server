export const sortHelper = (params, sortWhiteList: Array<string>) => {
  const sort =
    params.sort && (params.sort === "ASC" || params.sort === "DESC")
      ? params.sort
      : "ASC";
  const sortby = sortWhiteList.includes(params.sortby) ? params.sortby : "id";

  return { sort, sortby };
};
