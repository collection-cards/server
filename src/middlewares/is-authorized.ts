import { getRepository } from "typeorm";
import { jwtHelper } from "../helpers/jwt.helper";
import { User } from "../entities/User";

export const isAuthorized = async (req, res, next) => {
  if (!req.headers.authorization) {
    return res.status(401).send();
  }
  const tokenData = jwtHelper.verifyToken(req.headers.authorization);
  if (!tokenData.email) {
    return res.status(401).send();
  }
  const user = await getRepository(User)
  .createQueryBuilder("users")
  .where(`email=:email`, { email: tokenData.email })
  .getOne();
  if (!user) {
    return res.status(401).send();
  }
  req.currentUser = user;
  next();
};
