import express from "express";
import dotenv from "dotenv";
dotenv.config();
import nocache from "nocache";
import { connection } from "./entities/connection";
import { api } from "./api";

const PORT = process.env.PORT;

const app = express();

app.use(express.json());
app.use(nocache());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Credentials", "*");
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Authorization"
  );
  if ("OPTIONS" == req.method.toUpperCase()) {
    res.status(204).end();
  } else {
    next();
  }
});

app.use(async (req, res, next) => {
  await connection;
  next();
});
app.use(api);

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});
